﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



//Task 1 in PASCAL Format


class conversion   //contains variable that are common and required by inherited classes
{
    public int Int;
    public float Float;
    public char Char;
    public double Double;
    

}

class tofloat : conversion      // class that contains methods of type conversion of float. 
{
    public void conversion(float Float)                    //method that contains type conversion float.   
    {
        Console.WriteLine("below are the conversion of float type:");

        Console.WriteLine("float to int " + (int)Float);                     

        Console.WriteLine("float to double " + (double)Float);

    }
}

class toint:tofloat     //class contain method of type conversion of integer
{
    public void conversion(int Int)  
    {
        Console.WriteLine("below are the conversion of Integer type:");
        Float = Int;                                                      //implicit conversion
        Console.WriteLine("integer to float " + Float);
        Double = Int;                                                     //implicit conversion

        Console.WriteLine("integer to double " + Double);

        Char = Convert.ToChar(Int);                                      //explict
        Console.WriteLine("Integer to char " + Char);
    }
}

class tochar : toint     //for conversion of char to integer
{
    public void conversion(char Char)
    {
        Console.WriteLine("below are the conversion of char type:");
        Int = Char;                                               //implicit
        Console.WriteLine("char to int " + Int);
        Double = Char;                                            //implicit
        Console.WriteLine("char to double " + Double);
    }
}

class task1 : tochar    //class for conversion of String
{


    public void conversion(string String)
    {
        Console.WriteLine("below are the conversion of String type:");

        Int = Convert.ToInt32(String);   // explicit




        Console.WriteLine("String to int " + Int);



    }
}




//conversion method will work as overloaded

//also an example of runtime polimorphism




namespace firstapp
{
    class Program
    {
        static void Main(string[] args)
        {
            float FLoat = 3f;
            task1 Task1 = new task1();
            Task1.conversion("1234");
            Task1.conversion(2);
            Task1.conversion('a');
            Task1.conversion(FLoat);
            Console.ReadKey();
        }
    }
}
