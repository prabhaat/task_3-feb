﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    //Task 6 in camelCase

namespace task_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int? number;

            number = null;

            string checkNull;  

            checkNull = (number == null) ? "null" : number.ToString();  //checks whether number is null or not
           
            Console.WriteLine(checkNull);

            Console.ReadKey();

        }

    }

}
    

