﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//task 3 using Camelcase format


namespace task3
{
    class Program
    {
        
        //checks whether reference of two variable are same or not
        static void referenceCheck(string first, string second) //parameter as string
        {

            if (Object.ReferenceEquals(first, second))                 //in-bulit function "ReferenceEquals"
            {                                                         //checks whether passed variables
                Console.WriteLine("the references are Equal");       // sends true if they are, if not then false
                                                                    //have same reference or not and  
            }
            else
            {
                Console.WriteLine("the references are not Equal");
            }
        }

        static void referenceCheck(int first, int second)     //parameter as int
        {
            if (Object.ReferenceEquals(first, second))
            {
                Console.WriteLine("the references are Equal");
            }
            else
            {
                Console.WriteLine("the references are not Equal");
            }

        }

        static void Main(string[] args)
        {

            string firstString = "string reference";

            string secondString = firstString;

            //checking whether they have same reference
            referenceCheck(firstString, secondString);

            //changing the reference of firstString
            firstString = "reference changed";

            //checking for reference change
            referenceCheck(firstString, secondString);

            //performing above task using value type value

            //value type variable
            int firstNumber = 10;

            int secondNumber = firstNumber;

            referenceCheck(firstNumber, secondNumber);

            Console.ReadKey();

        }
    }
}
